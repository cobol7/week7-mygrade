       IDENTIFICATION DIVISION. 
       PROGRAM-ID. AVG-GRADE.
       AUTHOR. APANTRI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE VALUE HIGH-VALUE.
           05 SUBJECT-ID  PIC X(6).
           05 SUBJECT-NAME PIC X(50).
           05 SUBJECT-UNIT PIC 9(1).
           05 SUBJECT-GRADE PIC X(2).

       FD  AVG-FILE.
       01  AVG-DETAIL.
           05 AVG-NAME PIC X(18).
           05 AVG-GRADE-GPA  PIC 9.999.

       WORKING-STORAGE SECTION. 
       01  SUM-UNIT PIC 9(3).
       01  SUB-NUM-GRADE PIC 9(1)V9(1).
       01  SUM-GRADE-UNIT PIC 9(3)V9(1).
       01  SUM-GPA PIC 9(1)V9(3).

       01  SUM-SCI-UNIT PIC 9(3).
       01  SUB-SCI-NUM-GRADE PIC 9(1)V9(1).
       01  SUM-SCI-GRADE-UNIT PIC 9(3)V9(1).
       01  SUM-SCI-GPA PIC 9(1)V9(3).

       01  SUM-CS-UNIT PIC 9(3).
       01  SUB-CS-NUM-GRADE PIC 9(1)V9(1).
       01  SUM-CS-GRADE-UNIT PIC 9(3)V9(1).
       01  SUM-CS-GPA PIC 9(1)V9(3).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT GRADE-FILE
           
           PERFORM UNTIL END-OF-GRADE 
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE TO TRUE
              END-READ
              IF NOT END-OF-GRADE THEN

           PERFORM 001-AVG-GRADE  THRU 001-EXIT
           END-PERFORM
           
           DISPLAY "=================================================="
           DISPLAY "AVG-GRADE : " SUM-GPA 

           DISPLAY "=================================================="
           DISPLAY "AVG-SCI-GRADE : "  SUM-SCI-GPA

           DISPLAY "=================================================="
           DISPLAY "AVG-CS-GRADE : "  SUM-CS-GPA


           CLOSE GRADE-FILE 
           PERFORM 002-WRITE-FLIE THRU 002-EXIT 
           GOBACK 

           .

       001-AVG-GRADE.
           COMPUTE SUM-UNIT = SUM-UNIT + SUBJECT-UNIT 
           EVALUATE TRUE
              WHEN SUBJECT-GRADE = "A"   MOVE 4.0 To SUB-NUM-GRADE
              WHEN SUBJECT-GRADE = "B+"  MOVE 3.5 To SUB-NUM-GRADE
              WHEN SUBJECT-GRADE = "B"   MOVE 3.0 To SUB-NUM-GRADE
              WHEN SUBJECT-GRADE = "C+"  MOVE 2.5 To SUB-NUM-GRADE
              WHEN SUBJECT-GRADE = "C"   MOVE 2.0 To SUB-NUM-GRADE
              WHEN SUBJECT-GRADE = "D+"  MOVE 1.5 To SUB-NUM-GRADE
              WHEN SUBJECT-GRADE = "D"   MOVE 1.0 To SUB-NUM-GRADE
           END-EVALUATE 

           COMPUTE SUM-GRADE-UNIT 
              = (SUBJECT-UNIT * SUB-NUM-GRADE) + SUM-GRADE-UNIT 
           COMPUTE SUM-GPA = SUM-GRADE-UNIT / SUM-UNIT       
           
           IF SUBJECT-ID(1:1) IS EQUAL TO "3" THEN
              COMPUTE SUM-SCI-UNIT = SUM-SCI-UNIT + SUBJECT-UNIT 
              COMPUTE SUM-SCI-GRADE-UNIT
                 = (SUBJECT-UNIT * SUB-NUM-GRADE) + SUM-SCI-GRADE-UNIT
              COMPUTE SUM-SCI-GPA = SUM-SCI-GRADE-UNIT / SUM-SCI-UNIT 
           END-IF

           IF SUBJECT-ID(1:2) IS EQUAL TO "31" THEN
           COMPUTE SUM-CS-UNIT = SUM-CS-UNIT + SUBJECT-UNIT 
              COMPUTE SUM-CS-GRADE-UNIT
                 = (SUBJECT-UNIT * SUB-NUM-GRADE) + SUM-CS-GRADE-UNIT
              COMPUTE SUM-CS-GPA = SUM-CS-GRADE-UNIT / SUM-CS-UNIT

           END-IF

       .
       001-EXIT.
           EXIT. 
       002-WRITE-FLIE.
           OPEN OUTPUT AVG-FILE 
           MOVE "AVG-GRADE : " TO AVG-NAME
           MOVE SUM-GPA TO AVG-GRADE-GPA 
           WRITE AVG-DETAIL 

           MOVE "AVG-SCI-GRADE :  " TO AVG-NAME
           MOVE SUM-SCI-GPA TO AVG-GRADE-GPA 
           WRITE AVG-DETAIL 

           MOVE "AVG-CS-GRADE : " TO AVG-NAME
           MOVE SUM-CS-GPA  TO AVG-GRADE-GPA 
           WRITE AVG-DETAIL 

           CLOSE AVG-FILE 
       .

       002-EXIT.
           EXIT.
